#!/usr/bin/env -S scala-cli shebang

//> using scala 3.6
//> using toolkit 0.6.0
//> using option -new-syntax -explain -deprecation -feature -unchecked -Wunused:all

import scala.math.abs

case class Lists(left: List[Int], right: List[Int])

val input = os.read(os.pwd / "input.txt")
val pat = raw"(\d+)   (\d+)\n".r
val lists = pat
  .findAllMatchIn(input)
  .map(_.subgroups)
  .foldLeft(Lists(List(), List()))((lists, numbers) =>
    Lists(numbers(0).toInt +: lists.left, numbers(1).toInt +: lists.right)
  )
val sortedLists = Lists(lists.left.sorted, lists.right.sorted)
val totalDistance = sortedLists.left
  .lazyZip(sortedLists.right)
  .map((leftNum, rightNum) => abs(leftNum - rightNum))
  .sum
println(s"Answer for part 1: ${totalDistance}")

val totalSimilarityScore = sortedLists.left
  .map(leftNum =>
    leftNum * sortedLists.right.count(rightNum => leftNum == rightNum)
  )
  .sum
println(s"Answer for part 2: ${totalSimilarityScore}")

import 'dart:collection';
import 'dart:io';

void main(List<String> args) {
  final input = File('input.txt').readAsStringSync();
  final pat = RegExp(r'(\d+)   (\d+)\n');
  final lists = pat.allMatches(input).map((match) => match.groups([1, 2])).fold(
    (left: <int>[], right: <int>[]),
    (lists, numbers) {
      lists.left.add(int.parse(numbers[0]!));
      lists.right.add(int.parse(numbers[1]!));
      return lists;
    },
  );
  assert(lists.left.length == lists.right.length);
  lists.left.sort();
  lists.right.sort();
  final totalDistance = lists.left.indexed
      .map((idxNum) => (idxNum.$2 - lists.right[idxNum.$1]).abs())
      .reduce((totalDistance, distance) => totalDistance + distance);
  print('Answer for part 1: ${totalDistance}');

  final totalSimilarityScore = lists.left
      .map((numLeft) =>
          // This looks very slow (O(n^2)) but is fast in practice.
          numLeft * lists.right.where((numRight) => numLeft == numRight).length)
      .reduce((totalSimilarityScore, similarityScore) =>
          totalSimilarityScore + similarityScore);
  print('Answer for part 2: ${totalSimilarityScore}');
}

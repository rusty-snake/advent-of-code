#!/usr/bin/python3

import re

with open("input.txt") as input_file:
    input = input_file.read()

left_list = []
right_list = []
for match in re.finditer(r"(\d+)   (\d+)\n", input):
    left_list.append(int(match[1]))
    right_list.append(int(match[2]))
left_list.sort()
right_list.sort()
total_distance = sum(
    abs(left_num - right_num) for (left_num, right_num) in zip(left_list, right_list)
)
print(f"Answer for part 1: {total_distance}")

total_similarity_score = sum(
    left_num * right_list.count(left_num) for left_num in left_list
)
print(f"Answer for part 2: {total_similarity_score}")

use std::fs;
use std::num::ParseIntError;

struct Lists {
    left: Vec<i32>,
    right: Vec<i32>,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let input = fs::read_to_string("input.txt")?;
    let mut lists = input
        .lines()
        .map(|line| line.split_once("   ").unwrap())
        .map(|(left_num, right_num)| {
            Ok::<_, ParseIntError>((left_num.parse::<i32>()?, right_num.parse::<i32>()?))
        })
        .try_fold(
            Lists {
                left: Vec::new(),
                right: Vec::new(),
            },
            |mut lists, numbers| {
                let (left_num, right_num) = numbers?;
                lists.left.push(left_num);
                lists.right.push(right_num);
                Ok::<_, ParseIntError>(lists)
            },
        )?;
    lists.left.sort_unstable();
    lists.right.sort_unstable();

    let total_distance = lists
        .left
        .iter()
        .zip(&lists.right)
        .map(|(left_num, right_num)| (left_num - right_num).abs())
        .sum::<i32>();
    println!("Answer for part 1: {total_distance}");

    let total_similarity_score = lists
        .left
        .iter()
        .map(|left_num| {
            left_num
                * lists
                    .right
                    .iter()
                    .filter(|right_num| *right_num == left_num)
                    .count() as i32
        })
        .sum::<i32>();
    println!("Answer for part 2: {total_similarity_score}");

    Ok(())
}

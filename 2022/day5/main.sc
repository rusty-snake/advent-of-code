#!/usr/bin/env scala

import scala.collection.mutable.Stack
import scala.io.Source

case class Ship(stacks: List[Stack[Char]]):
  override def toString: String = stacks.map(_.mkString + "\n").mkString

case class MoveInstruction(quantity: Int, from: Int, to: Int)

@main def main(): Unit =
  val input = Source
    .fromFile("input.txt")
    .mkString
    .split("\n\n")

  val ship = Ship(
    input(0).linesIterator
      .takeWhile(!_.startsWith(" 1"))
      .map(_.grouped(4).map(_(1)))
      .foldLeft(List.fill(9)(List[Char]()))((stacks, cs) =>
        List.from(for (c, stack) <- (cs zip stacks) yield if c == ' ' then stack else c +: stack)
      ) // cs.zip(stacks).filter(_(0) != ' ').map((c, stack) => c +: stack)
      //.map(Stack.from(_))
      .map(s =>
        println(Stack.from(s).top)
        Stack.from(s)
      )
    )

  val program: List[MoveInstruction] = input(1).linesIterator
    .map(_.split(' '))
    .map(l => MoveInstruction(l(1).toInt, l(3).toInt, l(5).toInt))
    .toList

  println("Answer for part 1: " + part_1(ship, program))
  //println("Answer for part 2: " + part_2(input))

def part_1(ship: Ship, program: List[MoveInstruction]): String =
  for mi <- program do
    println(ship)
    println(mi)
    for _ <- 1 to mi.quantity do
      ship.stacks(mi.to - 1).push(ship.stacks(mi.from - 1).pop())
      println(ship.stacks(mi.from - 1).top)
  ""

//def part_2(input: List[String]): Int = 0

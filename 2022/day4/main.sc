#!/usr/bin/env scala

import scala.io.Source

@main def main(): Unit =
  val input = Source
    .fromFile("input.txt")
    .getLines
    .map(_.split(',').map(_.split('-').map(_.toInt)).map(id => id(0) to id(1)))
    .toList

  println("Answer for part 1: " + part_1(input))
  println("Answer for part 2: " + part_2(input))

def part_1(input: List[Array[Range.Inclusive]]): Int =
  input
    .count(p =>
      p(0).start <= p(1).start && p(0).end >= p(1).end
      || p(0).start >= p(1).start && p(0).end <= p(1).end
    )

def part_2(input: List[Array[Range.Inclusive]]): Int =
  input.count(p => (p(0) intersect p(1)).nonEmpty)

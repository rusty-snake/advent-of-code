#!/usr/bin/env scala

import scala.io.Source

@main def main(): Unit =
  val input = Source
    .fromFile("input.txt")
    .mkString
    .split("\n\n")
    .map(_.split("\n").map(_.toInt).sum)
    .sorted
    .reverse

  println("Answer for part 1: " + part_1(input))
  println("Answer for part 2: " + part_2(input))

def part_1(input: Array[Int]): Int = input(0)

def part_2(input: Array[Int]): Int = input.slice(0, 3).sum

use std::fs;
use std::str::FromStr;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let input = fs::read_to_string("input.txt")?;

    println!("Answer for part 1: {}", part_1(&input));
    println!("Answer for part 2: {}", part_2(&input));

    Ok(())
}

fn part_1(input: &str) -> u32 {
    input
        .split("\n\n")
        .map(|food_pack| {
            food_pack
                .lines()
                .map(|calories| u32::from_str(calories).unwrap())
                .sum()
        })
        .max()
        .unwrap()
}

fn part_2(input: &str) -> u32 {
    let mut calories_per_elf = input
        .split("\n\n")
        .map(|food_pack| {
            food_pack
                .lines()
                .map(|calories| u32::from_str(calories).unwrap())
                .sum()
        })
        .collect::<Vec<_>>();
    calories_per_elf.sort_unstable();
    calories_per_elf.as_slice()[calories_per_elf.len() - 3..]
        .iter()
        .sum()
}

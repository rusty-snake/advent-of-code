#![allow(clippy::identity_op)]

use std::fs;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let input = fs::read_to_string("input.txt")?
        .lines()
        .map(|line| {
            (
                line.chars().next().unwrap(),
                line.chars().next_back().unwrap(),
            )
        })
        .collect::<Vec<_>>();

    println!("Answer for part 1: {}", part_1(&input));
    println!("Answer for part 2: {}", part_2(&input));

    Ok(())
}

fn part_1(input: &[(char, char)]) -> u32 {
    input
        .iter()
        .map(|round| match round {
            ('A', 'X') => 1 + 3,
            ('A', 'Y') => 2 + 6,
            ('A', 'Z') => 3 + 0,
            ('B', 'X') => 1 + 0,
            ('B', 'Y') => 2 + 3,
            ('B', 'Z') => 3 + 6,
            ('C', 'X') => 1 + 6,
            ('C', 'Y') => 2 + 0,
            ('C', 'Z') => 3 + 3,
            _ => unreachable!(),
        })
        .sum()
}

fn part_2(input: &[(char, char)]) -> u32 {
    input
        .iter()
        .map(|round| match round {
            ('A', 'X') => 3 + 0,
            ('A', 'Y') => 1 + 3,
            ('A', 'Z') => 2 + 6,
            ('B', 'X') => 1 + 0,
            ('B', 'Y') => 2 + 3,
            ('B', 'Z') => 3 + 6,
            ('C', 'X') => 2 + 0,
            ('C', 'Y') => 3 + 3,
            ('C', 'Z') => 1 + 6,
            _ => unreachable!(),
        })
        .sum()
}

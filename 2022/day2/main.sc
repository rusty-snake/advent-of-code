#!/usr/bin/env scala

import scala.io.Source

@main def main(): Unit =
  val input = Source
    .fromFile("input.txt")
    .getLines
    .map(l => (l.charAt(0), l.charAt(2)))
    .toList

  println("Answer for part 1: " + part_1(input))
  println("Answer for part 2: " + part_2(input))

def part_1(input: List[(Char, Char)]): Int =
  input
    .map(_ match {
      case ('A', 'X') => 1 + 3
      case ('A', 'Y') => 2 + 6
      case ('A', 'Z') => 3 + 0
      case ('B', 'X') => 1 + 0
      case ('B', 'Y') => 2 + 3
      case ('B', 'Z') => 3 + 6
      case ('C', 'X') => 1 + 6
      case ('C', 'Y') => 2 + 0
      case ('C', 'Z') => 3 + 3
    })
    .sum

def part_2(input: List[(Char, Char)]): Int =
  input
    .map(_ match {
      case ('A', 'X') => 3 + 0
      case ('A', 'Y') => 1 + 3
      case ('A', 'Z') => 2 + 6
      case ('B', 'X') => 1 + 0
      case ('B', 'Y') => 2 + 3
      case ('B', 'Z') => 3 + 6
      case ('C', 'X') => 2 + 0
      case ('C', 'Y') => 3 + 3
      case ('C', 'Z') => 1 + 6
    })
    .sum

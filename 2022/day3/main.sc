#!/usr/bin/env scala

import scala.io.Source

case class Rucksack(leftCompartment: String, rightCompartment: String):
  def this(s: String) =
    this(s.substring(0, s.length / 2), s.substring(s.length / 2))

  def intersection = leftCompartment intersect rightCompartment

  override def toString = leftCompartment + rightCompartment

@main def main(): Unit =
  val input = Source
    .fromFile("input.txt")
    .getLines
    .map(new Rucksack(_))
    .toList

  println("Answer for part 1: " + part_1(input))
  println("Answer for part 2: " + part_2(input))

def part_1(input: List[Rucksack]): Int =
  input
    .map(_.intersection(0))
    .map(getPriority)
    .sum

def part_2(input: List[Rucksack]): Int =
  input
    .grouped(3)
    .map(g =>
      (g(0).toString intersect g(1).toString intersect g(2).toString)(0)
    )
    .map(getPriority)
    .sum

def getPriority(c: Char): Int = if c.isLower then c - 'a' + 1 else c - 'A' + 27

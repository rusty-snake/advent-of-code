import scala.io.StdIn

@main def main(): Unit =
  var counter = 0
  var last_measurement = 10_000
  try
    while true do
      val measurement = StdIn.readInt
      if measurement > last_measurement then counter += 1
      last_measurement = measurement
  catch
    case _: java.io.EOFException => ()
  println(s"Answer for part 1: $counter")

#!/usr/bin/node

const fs = require("fs");

let counter = 0;
let last_measurement = 10_000;
for (let line of fs.readFileSync(0).toString().split("\n")) {
	let measurement = parseInt(line);
	if (measurement > last_measurement) {
		counter++;
	}
	last_measurement = measurement;
}
console.log("Answer for part 1:", counter);

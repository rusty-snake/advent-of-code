use std::io::{stdin, BufRead};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let stdin = stdin();
    let stdin = stdin.lock();

    let mut counter = 0;
    let mut last_measurement = 10_000;
    for line in stdin.lines() {
        let measurement = line?.parse::<i32>()?;
        if measurement > last_measurement {
            counter += 1;
        }
        last_measurement = measurement;
    }
    println!("Answer for part 1: {}", counter);

    Ok(())
}

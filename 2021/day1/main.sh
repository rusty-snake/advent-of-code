#!/bin/bash

counter=0
last_measurement=10000
while read -r measurement; do
	if [[ $measurement -gt $last_measurement ]]; then
		(( counter++ ))
	fi
	last_measurement=$measurement
done
printf "Answer for part 1: %s\n" "$counter"

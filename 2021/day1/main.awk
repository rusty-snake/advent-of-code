#!/bin/awk --exec

BEGIN {
	counter = 0
	last_measurement = 10000
}
{
	measurement = int($0)
	if (measurement > last_measurement) {
		counter++
	}
	last_measurement = measurement
}
END {
	print "Answer for part 1: " counter
}

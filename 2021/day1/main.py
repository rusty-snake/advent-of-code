#!/usr/bin/python3

import collections
import fileinput
import sys


def main(argv: list[str]) -> int:
    # Part 1
    counter1 = 0
    # All measurements are in a range from 0 to 9999.
    # Therefore the first measurement will never be higher than 10,000.
    last_measurement = 10_000

    # Part 2
    counter2 = 0
    last_three_measurements = collections.deque([], 3)

    for line in fileinput.input():
        measurement = int(line)

        # Part 1
        if measurement > last_measurement:
            counter1 += 1
        last_measurement = measurement

        # Part 2
        if len(last_three_measurements) == 3:
            # FIXME: The sum for each window is computed twice
            last_window = sum(last_three_measurements)
            last_three_measurements.append(measurement)
            current_window = sum(last_three_measurements)
            if current_window > last_window:
                counter2 += 1
        else:
            last_three_measurements.append(measurement)

    print("Answer for part 1:", counter1)
    print("Answer for part 2:", counter2)

    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))

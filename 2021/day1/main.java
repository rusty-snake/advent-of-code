import java.util.Scanner;

public class main {
	public static void main(String[] args) {
		Scanner stdin = new Scanner(System.in);
		int counter = 0;
		int last_measurement = 10_000;
		while (stdin.hasNextLine()) {
			int measurement = Integer.parseInt(stdin.nextLine());
			if (measurement >= last_measurement) {
				counter++;
			}
			last_measurement = measurement;
		}
		System.out.printf("Answer for part 1: %d\n", counter);
	}
}

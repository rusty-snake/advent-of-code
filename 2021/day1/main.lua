#!/usr/bin/lua

counter = 0
last_measurement = 10000
for line in io.lines() do
	measurement = tonumber(line)
	if (measurement > last_measurement) then
		counter = counter + 1
	end
	last_measurement = measurement
end
print("Answer for part 1: " .. counter)

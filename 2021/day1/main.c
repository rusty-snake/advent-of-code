#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
	char buf[6];
	int counter = 0;
	int last_measurement = 10000;
	while (fgets(buf, sizeof(buf), stdin) != NULL) {
		int measurement = atoi(buf);
		if (measurement > last_measurement) {
			counter++;
		}
		last_measurement = measurement;
	}
	printf("Answer for part 1: %d\n", counter);
}

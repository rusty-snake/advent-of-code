#!/usr/bin/python3

import fileinput
import sys


def main(argv: list[str]) -> int:
    # Part 1
    position1 = {
        "horizontal": 0,
        "depth": 0,
    }

    # Part 2
    position2 = {
        "horizontal": 0,
        "depth": 0,
        "aim": 0,
    }

    for line in fileinput.input():
        direction, distance = line.split()
        distance = int(distance)

        # Part 1
        if direction == "forward":
            position1["horizontal"] += distance
        elif direction == "down":
            position1["depth"] += distance
        elif direction == "up":
            position1["depth"] -= distance
            if position1["depth"] < 0:
                print("I can fly")
        else:
            raise ValueError(f"Unknow direction: {direction}")

        # Part 2
        if direction == "forward":
            position2["horizontal"] += distance
            position2["depth"] += position2["aim"] * distance
        elif direction == "down":
            position2["aim"] += distance
        elif direction == "up":
            position2["aim"] -= distance
            if position1["depth"] < 0:
                print("I can fly")
        else:
            raise ValueError(f"Unknow direction: {direction}")


    print("Answer for part 1:", position1["horizontal"] * position1["depth"])
    print("Answer for part 2:", position2["horizontal"] * position2["depth"])

    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))

#!/usr/bin/env -S scala-cli shebang

//> using options -Wunused:all
//> using dep "com.lihaoyi::os-lib:0.9.2"

@main
def main() =
  val input = os.read(os.pwd / "input.txt")
  println("Answer for part 1: " + part1(input))
  println("Answer for part 2: " + part2(input))

case class CubesSet(val red: Int, val green: Int, val blue: Int)

object CubesSet:
  val Bag = CubesSet(red = 12, green = 13, blue = 14)

case class Game(id: Int, cubesSets: List[CubesSet])

def part1(input: String) =
  val bag = CubesSet(red = 12, green = 13, blue = 14)

  val games = {
    for line <- input.linesIterator yield
      val Array(head, tail) = line.split(": ", 2)
      val id = head.split(' ')(1).toInt
      val cubesSets = for rawCubesSet <- tail.split("; ") yield
        var r, g, b = 0
        for rawCubes <- rawCubesSet.split(", ") do
          rawCubes.split(' ') match
            case Array(amount, "red")   => r = amount.toInt
            case Array(amount, "green") => g = amount.toInt
            case Array(amount, "blue")  => b = amount.toInt
        CubesSet(r, g, b)
      Game(id, cubesSets.toList)
  }.toList

  games
    .filter(game =>
      game.cubesSets.forall(cubesSet =>
        cubesSet.red <= bag.red && cubesSet.green <= bag.green && cubesSet.blue <= bag.blue
      )
    )
    .map(_.id)
    .sum

def part2(input: String) =
  val games = {
    for line <- input.linesIterator yield
      val Array(head, tail) = line.split(": ", 2)
      val id = head.split(' ')(1).toInt
      val cubesSets = for rawCubesSet <- tail.split("; ") yield
        var r, g, b = 0
        for rawCubes <- rawCubesSet.split(", ") do
          rawCubes.split(' ') match
            case Array(amount, "red")   => r = amount.toInt
            case Array(amount, "green") => g = amount.toInt
            case Array(amount, "blue")  => b = amount.toInt
        CubesSet(r, g, b)
      Game(id, cubesSets.toList)
  }.toList

  games
    .map(game =>
      game.cubesSets.map(_.red).max
        * game.cubesSets.map(_.green).max
        * game.cubesSets.map(_.blue).max
    )
    .sum

#!/usr/bin/env -S scala-cli shebang

//> using dep "com.lihaoyi::os-lib:0.9.2"

import scala.util.matching.Regex

@main
def main() =
  val input = os.read(os.pwd / "input.txt")
  println("Answer for part 1: " + part1(input))
  println("Answer for part 2: " + part2(input))

def part1(input: String) =
  input.linesIterator
    .map(line => {
      val digits = line.filter(_.isDigit)
      s"${digits.head}${digits.last}".toInt
    })
    .sum

def part2(input: String) =
  val modifiedInput =
    """(?m)^(\D*?)(one|two|three|four|five|six|seven|eight|nine)""".r
      .replaceAllIn(
        input,
        _.group(2) match {
          case "one"   => "$11"
          case "two"   => "$12"
          case "three" => "$13"
          case "four"  => "$14"
          case "five"  => "$15"
          case "six"   => "$16"
          case "seven" => "$17"
          case "eight" => "$18"
          case "nine"  => "$19"
        }
      )
  val modifiedInput2 =
    """(?m)^(.*)(one|two|three|four|five|six|seven|eight|nine)""".r
      .replaceAllIn(
        modifiedInput,
        _.group(2) match {
          case "one"   => "$11"
          case "two"   => "$12"
          case "three" => "$13"
          case "four"  => "$14"
          case "five"  => "$15"
          case "six"   => "$16"
          case "seven" => "$17"
          case "eight" => "$18"
          case "nine"  => "$19"
        }
      )
  part1(modifiedInput2)
